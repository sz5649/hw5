# this is the broadcast class, used for testing
import zmq
import sys
import re
import time

def broadcast(port = '5556'):
	# port = '5556'
	if(len(sys.argv) > 1):
		port = sys.argv(1)

	# create a socket and bind to the port
	context = zmq.Context()
	socket = context.socket(zmq.PUB)
	socket.bind('tcp://*:%s' % port)

	fileName="fake_post_modernist_scholarship.txt"
	topic = 42

	# incrementally fetch some message
	message_data=""
	# impose word boundary to not include decimal points?
	sentence_end_finder=re.compile(r"[\.\?!]\b")

	with open(fileName) as myFile:
		for line in myFile:
			for word in line.split(' '):
				# the message will be in the form either a word, or a word with punctuation
				message_data = word
				socket.send('%d %s' % (topic, message_data));
				message_data = ''
				time.sleep(0.1)
			# print line


if __name__ == "__main__":
	broadcast('5556')