# this is the main script of HW5
import sys
import zmq
import re
import io
import time
import numpy as np
import matplotlib
matplotlib.use('TKAgg')
from matplotlib import pyplot as plt
from matplotlib import animation
from multiprocessing import Process
import multiprocessing
from PurpleReactor import *
from GreenReactor import *
from Broadcast import *

if __name__ == "__main__":
	speech_port = '5556'
	talk_port = '5558'
	# multiprocessing.set_start_method('spawn')
	Process(target = green, args = (speech_port, talk_port)).start()
	Process(target = purple, args = (speech_port, talk_port)).start()
	Process(target = broadcast, args = (speech_port,)).start()

