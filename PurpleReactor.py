import sys
import zmq
import re
import io
import time
import numpy as np
import matplotlib
matplotlib.use('TKAgg')
from matplotlib import pyplot as plt
from matplotlib import animation
import pyaudio
import wave

def purple(port="5556", talk_port = '5558'):
    # Socket to talk to server
    context = zmq.Context()
    socket = context.socket(zmq.SUB)
    talk_socket = context.socket(zmq.PAIR)
    # listening both to the oratory and the purple reactor
    print "Listening in to the broadcast..."
    socket.connect ("tcp://localhost:%s" % port)
    # use green as the server??!!

    # print "Listening to my peer Green..."
    # talk_socket.bind("tcp://*:%s" % talk_port)

    # create poller
    poller = zmq.Poller()
    poller.register(socket, zmq.POLLIN)
    # poller.register(talk_socket, zmq.POLLIN|zmq.POLLOUT)


    # Our message topic is 42
    topicfilter = "42"
    socket.setsockopt(zmq.SUBSCRIBE, topicfilter)

    # filenew = open("output.txt","w")

    numOfWords = 0
    totalSentence = 0
    totalWord = 0
    wordInSentence = 0
    wordPerSentence = 0
    maxWordPerSentence = 0
    minWordPerSentence = 1000

    charInWord = 0
    totalChar = 0
    maxCharLength = 0
    minCharLength = 1000
    avgCharLength = 0

    pronounList1 = ['yous guys', 'c++', 'shrubbery', 'star trek']
    pronounList2 = ['we', 'us', 'you', 'they', 'them']
    pronounList3 = ['y\'all', 'python', 'hedge', 'star wars']
    pronounList4 = ['i', 'me', 'you', 'he', 'him', 'she', 'her', 'it']

    numOfPronoun = 0
    pronounDict = {}
    for p in pronounList1:
        pronounDict[p] = 0

    lengthOfLongWord = 10
    lengthOfLongSentence = 20
    lengthOfShortWord = 5
    lengthOfShortSentence = 10

    numOfLongWord = 0
    numOfLongSentence = 0
    numOfShortWord = 0
    numOfShortSentence = 0

    collectPronoun = 0
    indiviPronoun = 0
    wordsGreenLike = 0
    wordsPurpleLike = 0
    pointOfGreen = 0
    pointOfPurple = 0

    dataround = []
    greenPoint = []
    purplePoint = []

    sentence_end_finder = re.compile(r"[\.\?!]")
    word_punctuation_finder = re.compile(r"[\.\?!,:;]")

    message_data = ''

    plt.axis([0, 300, -100, 100])
    plt.ion()
    plt.show()
    x = 0

    # send initial greetings to my peer
    # talk_socket.send(str(pointOfPurple))

    # open the audio file
    # chunk = 1024
    # wf = wave.open('horse.wav', 'rb')
    # audio = pyaudio.PyAudio()
    # stream = audio.open(format = audio.get_format_from_width(wf.getsampwidth()), \
    #                     channels = wf.getnchannels(), \
    #                     rate =  wf.getframerate(), \
    #                     output = True)
    # data = wf.readframes(chunk)


    # Process updates
    total_value = 0
    for update_nbr in range (300):
        socks = dict(poller.poll())
        # listen to the oratory
        if socket in socks and socks[socket] == zmq.POLLIN:
            string = socket.recv()
            words = string.split()[1:]
            # print " ".join(words)

            numberOfWords = len(words)
            totalWord += numberOfWords
            wordInSentence += numberOfWords

            if (sentence_end_finder.findall(string)):
                totalSentence += 1
                if (wordInSentence > maxWordPerSentence):
                    maxWordPerSentence = wordInSentence
                if (wordInSentence < minWordPerSentence):
                    minWordPerSentence = wordInSentence
                if (wordInSentence >= lengthOfLongSentence):
                    numOfLongSentence +=1
                if (wordInSentence <= lengthOfShortSentence):
                    numOfShortSentence += 1
                wordInSentence = 0
            if(totalSentence > 0):
                wordPerSentence = totalWord * 1.0 / totalSentence

            for word in words:
                # added this if to strip off words with a punctuation mark
                if (word_punctuation_finder.findall(word)):
                    word = word[:-2]
                    
                charInWord = len(word)
                totalChar += charInWord

                if(charInWord > maxCharLength):
                    maxCharLength = charInWord
                if(charInWord < minCharLength):
                    minCharLength = charInWord
                if(charInWord >= lengthOfShortWord):
                    numOfLongWord += 1
                if(charInWord <= lengthOfShortWord):
                    numOfShortWord += 1


                if (word in pronounList1):
                    wordsGreenLike += 1
                if (word in pronounList2):
                    collectPronoun += 1
                if (word in pronounList3):
                    wordsPurpleLike += 1
                if (word in pronounList4):
                    indiviPronoun += 1

            # pointOfGreen = wordsGreenLike + numOfLongSentence + numOfLongWord + collectPronoun - \
            #                 0.3*(wordsPurpleLike + numOfShortSentence + numOfShortWord + indiviPronoun)
            pointOfPurple = wordsPurpleLike + numOfShortSentence + numOfShortWord + indiviPronoun - \
                            0.3*(wordsGreenLike + numOfLongSentence + numOfLongWord + collectPronoun)


            avgCharLength = totalChar * 1.0 / totalWord
            # print avgCharLength
            printData = False
            if printData:
	            print "totalWord:", totalWord, '\n', \
	                  "totalSentence:", totalSentence, '\n', \
	                  "maxWordPerSentence:", maxWordPerSentence, '\n', \
	                  "minWordPerSentence:", minWordPerSentence, '\n', \
	                  "wordPerSentence", wordPerSentence, '\n', \
	                  "totalChar", totalChar, '\n', \
	                  "maxCharLength", maxCharLength, '\n', \
	                  "minCharLength", minCharLength, '\n', \
	                  "avgCharLength", avgCharLength, '\n', \
	                  "numOfLongSentence", numOfLongSentence, '\n', \
	                  "numOfShortSentence", numOfShortSentence, '\n', \
	                  "numOfLongWord", numOfLongWord, '\n', \
	                  "numOfShortWord", numOfShortWord, '\n', \
	                  "pointOfPurple", pointOfPurple

            x += 1
            # dataround.append(x)
            # greenPoint.append(pointOfGreen)
            purplePoint.append(pointOfPurple)
            plt.scatter(x, pointOfPurple)
            # plt.plot(dataround, greenPoint, '-go', dataround, purplePoint, '-mo')
            plt.plot(purplePoint, '-mo')
            plt.draw()

        # listen to the peer
        if talk_socket in socks and socks[talk_socket] == zmq.POLLIN:
            msg = talk_socket.recv()
            print msg
            point = int(msg)
            if (pointOfGreen > point):
                print "I like this guy/lady more."

            if time.time() % 3 == 0:
                talk_socket.send(str(pointOfPurple))

    # stream.close()
    # audio.terminate()


if __name__ == "__main__":
    purple("5556");
