# this is the module for text processing
import re

# count the number of words in a sentence

def wordPerSentence(filename):
	totalSentence = 0
	totalWord = 0
	wordpersentence = 0
	maxWordPerSentence = 0
	minWordPerSentence = 1000
	wordInSentence = 0
	sentence_end_finder=re.compile(r"[\.\?!]")
	message_data = ''
	with open(filename) as myFile:
		for line in myFile:
			for word in line.split():
				message_data=message_data + " " + word
				totalWord += 1
				wordInSentence += 1
				# print message data when a sentence ends
				if(sentence_end_finder.findall(word)):
					# print message_data
	                # update the max, min count
					if(wordInSentence > maxWordPerSentence):
						maxWordPerSentence = wordInSentence
	                if(wordInSentence < minWordPerSentence):
	                	minWordPerSentence = wordInSentence
	                # increment num of sentences
	                totalSentence += 1
	                message_data=""
	                wordInSentence = 0

	wordpersentence = totalWord * 1.0/ totalSentence
	return [totalWord, totalSentence, wordpersentence, maxWordPerSentence, minWordPerSentence]


            


def wordLength(filename):
	totalWord = 0
	totalChar = 0
	maxLength = 0
	minLength = 1000
	avgLength = 0
	sentence_end_finder=re.compile(r"[\.\?!,;:]")
	with open(filename) as myFile:
		for line in myFile:
			for word in line.split():
				totalWord += 1
				# if the word contains punctuation, trim it
				if(sentence_end_finder.findall(word)):
					word = word[:-2]
				totalChar += len(word)
				if(len(word) > maxLength):
					maxLength = len(word)
				if(len(word) < minLength):
					minLength = len(word)
	avgLength = totalChar * 1.0 / totalWord
	return [totalChar, totalWord, avgLength, maxLength, minLength]


# pronounList = ['i', 'me', 'we', 'you', 'he', 'him', 'she', 'her', 'they', 'them']
# pronounDict =  {'i', 'me', 'we', 'you', 'he', 'him', 'she', 'her', 'they', 'them'}
pronounList = ['is', 'the', 'this', 'of', 'and']

def commNounsNFreq(filename):
	# initialize the counter of the pronounDict 
	totalWord = 0
	pronounDict = {}
	sentence_end_finder =re.compile(r"[\.\?!,;:]")
	for p in pronounList:
		pronounDict[p] = 0

	with open(filename) as myFile:
		for line in myFile:
			for word in line.split():
				totalWord += 1
				# if the word contains punctuation, trim it
				if(sentence_end_finder.findall(word)):
					word = word[:-2]
				if(word in pronounList):
					pronounDict[word] += 1

	# find the two most frequent pronouns
	firstPro = ''
	secondPro = ''
	firstOccur = 0
	secondOccur = 0

	for key in pronounDict:
		if (pronounDict[key] >= firstOccur):
			firstOccur, secondOccur = pronounDict[key], firstOccur
			firstPro, secondPro = key, firstPro

		elif (pronounDict[key] > secondOccur):
			secondPro = key
			secondOccur = pronounDict[key]

	return {firstPro: firstOccur, secondPro: secondOccur}



# testing code
if __name__=="__main__":
    filename = "fake_post_modernist_scholarship.txt"
    [totalWord, totalSentence, wordpersentence, maxWordPerSentence, minWordPerSentence] = wordPerSentence(filename)
    print [totalWord, totalSentence, wordpersentence, maxWordPerSentence, minWordPerSentence]

    [totalChar, totalWord, avgLength, maxLength, minLength] = wordLength(filename)
    print [totalChar, totalWord, avgLength, maxLength, minLength]

    twoPronouns = commNounsNFreq(filename)
    print twoPronouns
